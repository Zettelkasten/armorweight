![banner.png](http://dev.bukkit.org/media/images/75/972/banner-transparent.png)

# ArmorWeight Repository #

### Important links ###

* [Bukkit Dev site](http://dev.bukkit.org/bukkit-plugins/armorweight/)
* [Localization](http://dev.bukkit.org/bukkit-plugins/armorweight/localization/)
* [Issue tracker](https://bitbucket.org/Zettelkasten/armorweight/issues?status=new&status=open)

### How can I help? ###

* Report issues
* Translate the plugin ([Localization](http://dev.bukkit.org/bukkit-plugins/armorweight/localization/))
* Document the code for JavaDoc
* Contribute: Fix issues / add features

### How do I get set up? ###

```
#!
https://bitbucket.org/Zettelkasten/armorweight
```

* Fork the repository
* Install the appropriate Bukkit version ([Bukkit build list](http://dl.bukkit.org/downloads/craftbukkit/))
* Commit changes
* When done handling issue, create pull request back to this repository