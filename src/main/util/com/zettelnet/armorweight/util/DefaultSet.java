package com.zettelnet.armorweight.util;
public class DefaultSet {

	public static void main(String[] args) {
		// Metallurgy Items
		System.out.println(generateMetallurgy("copper", "astral_silver", "steel", "haderoth", "quicksilver", "bronze", "angmallen", "deep_iron", "black_steel", "prometheum", "oureclase", "carmot", "orichalcum", "damascus_steel", "mithril", "hepatizon", "desichalkos", "celenegil", "eximite", "kalendrite", "electrum", "vulcanite", "sanguinite", "inolashite", "amordrine", "shadow_steel", "silver", "brass", "platinum", "ceruclase", "midasium", "shadow_iron", "ignatius", "atlarus", "adamantine", "vyroxeres", "tartarite"));
	}

	public static String generateMetallurgy(String matName) {
		return String.format("registerIfAvailable(\"metallurgy_%1$s\", \"METALLURGY_METALLURGY_%2$s_HELMET\", \"METALLURGY_METALLURGY_%2$s_CHESTPLATE\", \"METALLURGY_METALLURGY_%2$s_LEGGINGS\", \"METALLURGY_METALLURGY_%2$s_BOOTS\");", matName, matName.toUpperCase());
	}

	public static String generateMetallurgy(String... matNames) {
		StringBuilder result = new StringBuilder();
		for (String matName : matNames) {
			result.append(generateMetallurgy(matName));
			result.append("\n");
		}
		return result.toString();
	}
}
