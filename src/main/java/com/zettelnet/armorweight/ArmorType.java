package com.zettelnet.armorweight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ArmorType {

	private final static Map<String, ArmorType> registeredNames = new HashMap<String, ArmorType>();
	private final static Map<Material, ArmorType> registeredMaterials = new HashMap<Material, ArmorType>();

	public static Collection<ArmorType> values() {
		return Collections.unmodifiableCollection(registeredNames.values());
	}

	public static ArmorType valueOf(String name) {
		return registeredNames.get(name.toLowerCase());
	}

	public static boolean contains(String name) {
		return registeredNames.containsKey(name);
	}

	public static ArmorType valueOf(Material material) {
		ArmorType type = registeredMaterials.get(material);
		return type == null ? AIR : type;
	}

	public static boolean contains(Material material) {
		return registeredMaterials.containsKey(material);
	}

	public static void register(ArmorType type) {
		Validate.notNull(type, "Type cannot be null");
		Validate.isTrue(!registeredNames.containsKey(type.getName().toLowerCase()), "Type name already registered");
		registeredNames.put(type.getName().toLowerCase(), type);
		for (Material mat : type.getMaterial()) {
			if (registeredMaterials.containsKey(mat)) {
				// Will override silently event though material already exists
			}
			registeredMaterials.put(mat, type);
		}
	}

	public static boolean registerIfAvailable(String name, String... materialNames) {
		Validate.notNull(name, "Type name cannot be null");
		Validate.notNull(materialNames, "Material names cannot be null");

		List<Material> materials = new ArrayList<Material>(materialNames.length);
		for (String materialName : materialNames) {
			try {
				Material material = Material.valueOf(materialName);
				materials.add(material);
			} catch (IllegalArgumentException e) {
			}
		}
		register(new ArmorType(name, materials.toArray(new Material[materials.size()])));
		return true;
	}

	public static void reset() {
		registeredNames.clear();
		registeredMaterials.clear();

		// Vanilla Armor
		register(AIR);
		registerIfAvailable("leather", "LEATHER_HELMET", "LEATHER_CHESTPLATE", "LEATHER_LEGGINGS", "LEATHER_BOOTS");
		registerIfAvailable("gold", "GOLDEN_HELMET", "GOLDEN_CHESTPLATE", "GOLDEN_LEGGINGS", "GOLDEN_BOOTS", "GOLDEN_HORSE_ARMOR",
				"GOLD_HELMET", "GOLD_CHESTPLATE", "GOLD_LEGGINGS", "GOLD_BOOTS", "GOLD_BARDING");
		registerIfAvailable("chainmail", "CHAINMAIL_HELMET", "CHAINMAIL_CHESTPLATE", "CHAINMAIL_LEGGINGS", "CHAINMAIL_BOOTS");
		registerIfAvailable("iron", "IRON_HELMET", "IRON_CHESTPLATE", "IRON_LEGGINGS", "IRON_BOOTS", "IRON_HORSE_ARMOR", "IRON_BARDING");
		registerIfAvailable("diamond", "DIAMOND_HELMET", "DIAMOND_CHESTPLATE", "DIAMOND_LEGGINGS", "DIAMOND_BOOTS", "DIAMOND_HORSE_ARMOR", "DIAMOND_BARDING");

		// Cauldron Mod Armor

		// Metallurgy Armor
		// http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1278061
		registerIfAvailable("metallurgy_copper", "METALLURGY_METALLURGY_COPPER_HELMET", "METALLURGY_METALLURGY_COPPER_CHESTPLATE", "METALLURGY_METALLURGY_COPPER_LEGGINGS", "METALLURGY_METALLURGY_COPPER_BOOTS");
		registerIfAvailable("metallurgy_astral_silver", "METALLURGY_METALLURGY_ASTRAL_SILVER_HELMET", "METALLURGY_METALLURGY_ASTRAL_SILVER_CHESTPLATE", "METALLURGY_METALLURGY_ASTRAL_SILVER_LEGGINGS", "METALLURGY_METALLURGY_ASTRAL_SILVER_BOOTS");
		registerIfAvailable("metallurgy_steel", "METALLURGY_METALLURGY_STEEL_HELMET", "METALLURGY_METALLURGY_STEEL_CHESTPLATE", "METALLURGY_METALLURGY_STEEL_LEGGINGS", "METALLURGY_METALLURGY_STEEL_BOOTS");
		registerIfAvailable("metallurgy_haderoth", "METALLURGY_METALLURGY_HADEROTH_HELMET", "METALLURGY_METALLURGY_HADEROTH_CHESTPLATE", "METALLURGY_METALLURGY_HADEROTH_LEGGINGS", "METALLURGY_METALLURGY_HADEROTH_BOOTS");
		registerIfAvailable("metallurgy_quicksilver", "METALLURGY_METALLURGY_QUICKSILVER_HELMET", "METALLURGY_METALLURGY_QUICKSILVER_CHESTPLATE", "METALLURGY_METALLURGY_QUICKSILVER_LEGGINGS", "METALLURGY_METALLURGY_QUICKSILVER_BOOTS");
		registerIfAvailable("metallurgy_bronze", "METALLURGY_METALLURGY_BRONZE_HELMET", "METALLURGY_METALLURGY_BRONZE_CHESTPLATE", "METALLURGY_METALLURGY_BRONZE_LEGGINGS", "METALLURGY_METALLURGY_BRONZE_BOOTS");
		registerIfAvailable("metallurgy_angmallen", "METALLURGY_METALLURGY_ANGMALLEN_HELMET", "METALLURGY_METALLURGY_ANGMALLEN_CHESTPLATE", "METALLURGY_METALLURGY_ANGMALLEN_LEGGINGS", "METALLURGY_METALLURGY_ANGMALLEN_BOOTS");
		registerIfAvailable("metallurgy_deep_iron", "METALLURGY_METALLURGY_DEEP_IRON_HELMET", "METALLURGY_METALLURGY_DEEP_IRON_CHESTPLATE", "METALLURGY_METALLURGY_DEEP_IRON_LEGGINGS", "METALLURGY_METALLURGY_DEEP_IRON_BOOTS");
		registerIfAvailable("metallurgy_black_steel", "METALLURGY_METALLURGY_BLACK_STEEL_HELMET", "METALLURGY_METALLURGY_BLACK_STEEL_CHESTPLATE", "METALLURGY_METALLURGY_BLACK_STEEL_LEGGINGS", "METALLURGY_METALLURGY_BLACK_STEEL_BOOTS");
		registerIfAvailable("metallurgy_prometheum", "METALLURGY_METALLURGY_PROMETHEUM_HELMET", "METALLURGY_METALLURGY_PROMETHEUM_CHESTPLATE", "METALLURGY_METALLURGY_PROMETHEUM_LEGGINGS", "METALLURGY_METALLURGY_PROMETHEUM_BOOTS");
		registerIfAvailable("metallurgy_oureclase", "METALLURGY_METALLURGY_OURECLASE_HELMET", "METALLURGY_METALLURGY_OURECLASE_CHESTPLATE", "METALLURGY_METALLURGY_OURECLASE_LEGGINGS", "METALLURGY_METALLURGY_OURECLASE_BOOTS");
		registerIfAvailable("metallurgy_carmot", "METALLURGY_METALLURGY_CARMOT_HELMET", "METALLURGY_METALLURGY_CARMOT_CHESTPLATE", "METALLURGY_METALLURGY_CARMOT_LEGGINGS", "METALLURGY_METALLURGY_CARMOT_BOOTS");
		registerIfAvailable("metallurgy_orichalcum", "METALLURGY_METALLURGY_ORICHALCUM_HELMET", "METALLURGY_METALLURGY_ORICHALCUM_CHESTPLATE", "METALLURGY_METALLURGY_ORICHALCUM_LEGGINGS", "METALLURGY_METALLURGY_ORICHALCUM_BOOTS");
		registerIfAvailable("metallurgy_damascus_steel", "METALLURGY_METALLURGY_DAMASCUS_STEEL_HELMET", "METALLURGY_METALLURGY_DAMASCUS_STEEL_CHESTPLATE", "METALLURGY_METALLURGY_DAMASCUS_STEEL_LEGGINGS", "METALLURGY_METALLURGY_DAMASCUS_STEEL_BOOTS");
		registerIfAvailable("metallurgy_mithril", "METALLURGY_METALLURGY_MITHRIL_HELMET", "METALLURGY_METALLURGY_MITHRIL_CHESTPLATE", "METALLURGY_METALLURGY_MITHRIL_LEGGINGS", "METALLURGY_METALLURGY_MITHRIL_BOOTS");
		registerIfAvailable("metallurgy_hepatizon", "METALLURGY_METALLURGY_HEPATIZON_HELMET", "METALLURGY_METALLURGY_HEPATIZON_CHESTPLATE", "METALLURGY_METALLURGY_HEPATIZON_LEGGINGS", "METALLURGY_METALLURGY_HEPATIZON_BOOTS");
		registerIfAvailable("metallurgy_desichalkos", "METALLURGY_METALLURGY_DESICHALKOS_HELMET", "METALLURGY_METALLURGY_DESICHALKOS_CHESTPLATE", "METALLURGY_METALLURGY_DESICHALKOS_LEGGINGS", "METALLURGY_METALLURGY_DESICHALKOS_BOOTS");
		registerIfAvailable("metallurgy_celenegil", "METALLURGY_METALLURGY_CELENEGIL_HELMET", "METALLURGY_METALLURGY_CELENEGIL_CHESTPLATE", "METALLURGY_METALLURGY_CELENEGIL_LEGGINGS", "METALLURGY_METALLURGY_CELENEGIL_BOOTS");
		registerIfAvailable("metallurgy_eximite", "METALLURGY_METALLURGY_EXIMITE_HELMET", "METALLURGY_METALLURGY_EXIMITE_CHESTPLATE", "METALLURGY_METALLURGY_EXIMITE_LEGGINGS", "METALLURGY_METALLURGY_EXIMITE_BOOTS");
		registerIfAvailable("metallurgy_kalendrite", "METALLURGY_METALLURGY_KALENDRITE_HELMET", "METALLURGY_METALLURGY_KALENDRITE_CHESTPLATE", "METALLURGY_METALLURGY_KALENDRITE_LEGGINGS", "METALLURGY_METALLURGY_KALENDRITE_BOOTS");
		registerIfAvailable("metallurgy_electrum", "METALLURGY_METALLURGY_ELECTRUM_HELMET", "METALLURGY_METALLURGY_ELECTRUM_CHESTPLATE", "METALLURGY_METALLURGY_ELECTRUM_LEGGINGS", "METALLURGY_METALLURGY_ELECTRUM_BOOTS");
		registerIfAvailable("metallurgy_vulcanite", "METALLURGY_METALLURGY_VULCANITE_HELMET", "METALLURGY_METALLURGY_VULCANITE_CHESTPLATE", "METALLURGY_METALLURGY_VULCANITE_LEGGINGS", "METALLURGY_METALLURGY_VULCANITE_BOOTS");
		registerIfAvailable("metallurgy_sanguinite", "METALLURGY_METALLURGY_SANGUINITE_HELMET", "METALLURGY_METALLURGY_SANGUINITE_CHESTPLATE", "METALLURGY_METALLURGY_SANGUINITE_LEGGINGS", "METALLURGY_METALLURGY_SANGUINITE_BOOTS");
		registerIfAvailable("metallurgy_inolashite", "METALLURGY_METALLURGY_INOLASHITE_HELMET", "METALLURGY_METALLURGY_INOLASHITE_CHESTPLATE", "METALLURGY_METALLURGY_INOLASHITE_LEGGINGS", "METALLURGY_METALLURGY_INOLASHITE_BOOTS");
		registerIfAvailable("metallurgy_amordrine", "METALLURGY_METALLURGY_AMORDRINE_HELMET", "METALLURGY_METALLURGY_AMORDRINE_CHESTPLATE", "METALLURGY_METALLURGY_AMORDRINE_LEGGINGS", "METALLURGY_METALLURGY_AMORDRINE_BOOTS");
		registerIfAvailable("metallurgy_shadow_steel", "METALLURGY_METALLURGY_SHADOW_STEEL_HELMET", "METALLURGY_METALLURGY_SHADOW_STEEL_CHESTPLATE", "METALLURGY_METALLURGY_SHADOW_STEEL_LEGGINGS", "METALLURGY_METALLURGY_SHADOW_STEEL_BOOTS");
		registerIfAvailable("metallurgy_silver", "METALLURGY_METALLURGY_SILVER_HELMET", "METALLURGY_METALLURGY_SILVER_CHESTPLATE", "METALLURGY_METALLURGY_SILVER_LEGGINGS", "METALLURGY_METALLURGY_SILVER_BOOTS");
		registerIfAvailable("metallurgy_brass", "METALLURGY_METALLURGY_BRASS_HELMET", "METALLURGY_METALLURGY_BRASS_CHESTPLATE", "METALLURGY_METALLURGY_BRASS_LEGGINGS", "METALLURGY_METALLURGY_BRASS_BOOTS");
		registerIfAvailable("metallurgy_platinum", "METALLURGY_METALLURGY_PLATINUM_HELMET", "METALLURGY_METALLURGY_PLATINUM_CHESTPLATE", "METALLURGY_METALLURGY_PLATINUM_LEGGINGS", "METALLURGY_METALLURGY_PLATINUM_BOOTS");
		registerIfAvailable("metallurgy_ceruclase", "METALLURGY_METALLURGY_CERUCLASE_HELMET", "METALLURGY_METALLURGY_CERUCLASE_CHESTPLATE", "METALLURGY_METALLURGY_CERUCLASE_LEGGINGS", "METALLURGY_METALLURGY_CERUCLASE_BOOTS");
		registerIfAvailable("metallurgy_midasium", "METALLURGY_METALLURGY_MIDASIUM_HELMET", "METALLURGY_METALLURGY_MIDASIUM_CHESTPLATE", "METALLURGY_METALLURGY_MIDASIUM_LEGGINGS", "METALLURGY_METALLURGY_MIDASIUM_BOOTS");
		registerIfAvailable("metallurgy_shadow_iron", "METALLURGY_METALLURGY_SHADOW_IRON_HELMET", "METALLURGY_METALLURGY_SHADOW_IRON_CHESTPLATE", "METALLURGY_METALLURGY_SHADOW_IRON_LEGGINGS", "METALLURGY_METALLURGY_SHADOW_IRON_BOOTS");
		registerIfAvailable("metallurgy_ignatius", "METALLURGY_METALLURGY_IGNATIUS_HELMET", "METALLURGY_METALLURGY_IGNATIUS_CHESTPLATE", "METALLURGY_METALLURGY_IGNATIUS_LEGGINGS", "METALLURGY_METALLURGY_IGNATIUS_BOOTS");
		registerIfAvailable("metallurgy_atlarus", "METALLURGY_METALLURGY_ATLARUS_HELMET", "METALLURGY_METALLURGY_ATLARUS_CHESTPLATE", "METALLURGY_METALLURGY_ATLARUS_LEGGINGS", "METALLURGY_METALLURGY_ATLARUS_BOOTS");
		registerIfAvailable("metallurgy_adamantine", "METALLURGY_METALLURGY_ADAMANTINE_HELMET", "METALLURGY_METALLURGY_ADAMANTINE_CHESTPLATE", "METALLURGY_METALLURGY_ADAMANTINE_LEGGINGS", "METALLURGY_METALLURGY_ADAMANTINE_BOOTS");
		registerIfAvailable("metallurgy_vyroxeres", "METALLURGY_METALLURGY_VYROXERES_HELMET", "METALLURGY_METALLURGY_VYROXERES_CHESTPLATE", "METALLURGY_METALLURGY_VYROXERES_LEGGINGS", "METALLURGY_METALLURGY_VYROXERES_BOOTS");
		registerIfAvailable("metallurgy_tartarite", "METALLURGY_METALLURGY_TARTARITE_HELMET", "METALLURGY_METALLURGY_TARTARITE_CHESTPLATE", "METALLURGY_METALLURGY_TARTARITE_LEGGINGS", "METALLURGY_METALLURGY_TARTARITE_BOOTS");

		// IndustrialCraft 2
		// http://industrial-craft.net
		registerIfAvailable("ic2_bronze", "IC2_ITEMARMORBRONZEHELMET", "IC2_ITEMARMORBRONZECHESTPLATE", "IC2_ITEMARMORBRONZELEGS", "IC2_ITEMARMORBRONZEBOOTS");
		registerIfAvailable("ic2_nano", "IC2_ITEMARMORNANOHELMET", "IC2_ITEMARMORNANOCHESTPLATE", "IC2_ITEMARMORNANOLEGS", "ITEMARMORNANOBOOTS");
		registerIfAvailable("ic2_quantum", "IC2_ITEMARMORQUANTUMHELMET", "IC2_ITEMARMORQUANTUMCHESTPLATE", "IC2_ITEMARMORQUANTUMLEGS", "IC2_ITEMARMORQUANTUMBOOTS");
		registerIfAvailable("ic2_hazmat", "IC2_ITEMARMORHAZMATHELMET", "IC2_ITEMARMORHAZMATCHSTPLATE", "IC2_ITEMARMORHAZMATLEGS", "IC2_ITEMARMORHAZMATBOOTS");

		// HarvestCraft
		// http://www.curse.com/mc-mods/minecraft/221857-pams-harvestcraft
		registerIfAvailable("harvestcraft_hardened_leather", "HARVESTCRAFT_HARDENEDLEATHERHELMITEM", "HARVESTCRAFT_HARDENEDLEATHERCHESTITEM", "HARVESTCRAFT_HARDENEDLEATHERLEGGINGSITEM", "HARVESTCRAFT_HARDENEDLEATHERBOOTSITEM");

		// DesertCraft
		// http://www.curse.com/mc-mods/minecraft/221911-pams-desertcraft
		registerIfAvailable("desertcraft_cactus", "DESERTCRAFT_CACTUS_HELM", "DESERTCRAFT_CACTUS_CHEST", "DESERTCRAFT_CACTUS_LEGS", "DESERTCRAFT_CACTUS_BOOTS");

		// Tinkers Construct
		// http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1287648-tinkers-construct
		registerIfAvailable("tconstruct_wood", "TCONSTRUCT_HELMETWOOD", "TCONSTRUCT_CHESTPLATEWOOD", "TCONSTRUCT_LEGGINGSWOOD", "TCONSTRUCT_BOOTSWOOD");
		registerIfAvailable("tconstruct_travel", "TCONSTRUCT_TRAVELGOGGLES", "TCONSTRUCT_TRAVELVEST", "TCONSTRUCT_TRAVELWINGS", "TCONSTRUCT_TRAVELBOOTS");

		// CustomNPCs
		registerIfAvailable("customnpcs_cowleather", "CUSTOMNPCS_NPCCOWLEATHERHEAD", "CUSTOMNPCS_NPCCOWLEATHERCHEST", "CUSTOMNPCS_NPCCOWLEATHERLEGS", "CUSTOMNPCS_NPCCOWLEATHERBOOTS");
		registerIfAvailable("customnpcs_nanorum", "CUSTOMNPCS_NPCNANORUMHEAD", "CUSTOMNPCS_NPCNANORUMCHEST", "CUSTOMNPCS_NPCNANORUMLEGS", "CUSTOMNPCS_NPCNANORUMBOOTS");
		registerIfAvailable("customnpcs_tactical", "CUSTOMNPCS_NPCTACTICALHEAD", "CUSTOMNPCS_NPCTACTICALCHEST");
		registerIfAvailable("customnpcs_full_leather", "CUSTOMNPCS_NPCFULLLEATHERHEAD", "CUSTOMNPCS_NPCFULLLEATHERCHEST");
		registerIfAvailable("customnpcs_full_gold", "CUSTOMNPCS_NPCFULLGOLDHEAD", "CUSTOMNPCS_NPCFULLGOLDCHEST");
		registerIfAvailable("customnpcs_full_iron", "CUSTOMNPCS_NPCFULLIRONHEAD", "CUSTOMNPCS_NPCFULLIRONCHEST");
		registerIfAvailable("customnpcs_full_diamond", "CUSTOMNPCS_NPCFULLDIAMONDHEAD", "CUSTOMNPCS_NPCFULLDIAMONDCHEST");
		registerIfAvailable("customnpcs_full_bronze", "CUSTOMNPCS_NPCFULLBRONZEHEAD", "CUSTOMNPCS_NPCFULLBRONZECHEST", "CUSTOMNPCS_NPCFULLBRONZELEGGINGS", "CUSTOMNPCS_NPCFULLBRONZEBOOTS");
		registerIfAvailable("customnpcs_full_emerald", "CUSTOMNPCS_NPCFULLEMERALDHEAD", "CUSTOMNPCS_NPCFULLEMERALDCHEST", "CUSTOMNPCS_NPCFULLEMERALDLEGGINGS", "CUSTOMNPCS_NPCFULLEMERALDBOOTS");
		registerIfAvailable("customnpcs_full_wooden", "CUSTOMNPCS_NPCFULLWOODENHEAD", "CUSTOMNPCS_NPCFULLWOODENCHEST", "CUSTOMNPCS_NPCFULLWOODENLEGGINGS", "CUSTOMNPCS_NPCFULLWOODENBOOTS");
		registerIfAvailable("customnpcs_tuxedo", "CUSTOMNPCS_NPCTUXEDOCHEST", "CUSTOMNPCS_NPCTUXEDOPANTS", "CUSTOMNPCS_NPCTUXEDOBOTTOM");
		registerIfAvailable("customnpcs_wizard", "CUSTOMNPCS_NPCWIZARDCHEST", "CUSTOMNPCS_NPCWIZARDCHEST", "CUSTOMNPCS_NPCWIZARDPANTS");
		registerIfAvailable("customnpcs_assassin", "CUSTOMNPCS_NPCASSASSINHEAD", "CUSTOMNPCS_NPCASSASSINCHEST", "CUSTOMNPCS_NPCASSASSINLEGGINGS", "CUSTOMNPCS_NPCASSASSINBOOTS");
		registerIfAvailable("customnpcs_soldier", "CUSTOMNPCS_NPCSOLDIERHEAD", "CUSTOMNPCS_NPCSOLDIERCHEST", "CUSTOMNPCS_NPCSOLDIERLEGS", "CUSTOMNPCS_NPCSOLDIERBOTTOM");
		registerIfAvailable("customnpcs_x407", "CUSTOMNPCS_NPCX407HEAD", "CUSTOMNPCS_NPCX407CHEST", "CUSTOMNPCS_NPCX407LEGS", "CUSTOMNPCS_NPCX407BOOTS");
		registerIfAvailable("customnpcs_commissar", "CUSTOMNPCS_NPCCOMMISSARHEAD", "CUSTOMNPCS_NPCCOMMISSARCHEST", "CUSTOMNPCS_NPCCOMMISSARLEGS", "CUSTOMNPCS_NPCCOMMISSARBOTTOM");
		registerIfAvailable("customnpcs_infantry", "CUSTOMNPCS_NPCINFANTRYHELMET");
		registerIfAvailable("customnpcs_ninja", "CUSTOMNPCS_NPCNINJAHEAD", "CUSTOMNPCS_NPCNINJACHEST", "CUSTOMNPCS_NPCNINJAPANTS");
		registerIfAvailable("customnpcs_officer", "CUSTOMNPCS_NPCOFFICERCHEST");
		registerIfAvailable("customnpcs_bandit", "CUSTOMNPCS_NPCBANDITMASK");
		registerIfAvailable("customnpcs_crown", "CUSTOMNPCS_NPCCROWN");
		registerIfAvailable("customnpcs_crown2", "CUSTOMNPCS_NPCCROWN2");
		registerIfAvailable("customnpcs_papercrown", "CUSTOMNPCS_NPCPAPERCROWN");
		registerIfAvailable("customnpcs_iron_skirt", "CUSTOMNPCS_NPCIRONSKIRT");
		registerIfAvailable("customnpcs_chain_skirt", "CUSTOMNPCS_NPCCHAINSKIRT");
		registerIfAvailable("customnpcs_leather_skirt", "CUSTOMNPCS_NPCLEATHERSKIRT");
		registerIfAvailable("customnpcs_diamond_skirt", "CUSTOMNPCS_NPCDIAMONDSKIRT");
		registerIfAvailable("customnpcs_gold_skirt", "CUSTOMNPCS_NPCGOLDSKIRT");

	}

	public final static ArmorType AIR = new ArmorType("none", Material.AIR);
	static {
		AIR.setWeight(0F);
	}
	
	private final String name;
	private final Material[] materials;

	private double weight;

	public ArmorType(Material material) {
		this(material.toString().toLowerCase(), material);
	}

	public ArmorType(String name, Material... contentMaterials) {
		this.name = name;
		this.materials = contentMaterials;
		this.weight = 1F;
	}

	public String getName() {
		return name;
	}

	public Material[] getMaterial() {
		return materials;
	}

	@Deprecated
	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getWeight(ItemStack item) {
		return getWeight();
	}

	@Override
	public String toString() {
		return "ArmorType [name=" + name + ",materials=" + Arrays.toString(materials) + "]";
	}

	public static double getItemWeight(ItemStack item) {
		return valueOf(item == null ? null : item.getType()).getWeight(item);
	}
}
