package com.zettelnet.armorweight.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerWeightChangeEvent extends PlayerEvent {

	private static final HandlerList handlers = new HandlerList();

	private double oldWeight, newWeight;

	public PlayerWeightChangeEvent(final Player player, double oldWeight, double newWeight) {
		super(player);

		this.oldWeight = oldWeight;
		this.newWeight = newWeight;
	}

	public double getOldWeight() {
		return oldWeight;
	}

	public double getNewWeight() {
		return newWeight;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public String toString() {
		return "PlayerWeightChangeEvent [player=" + getPlayer() + ", oldWeight=" + oldWeight + ", newWeight=" + newWeight + "]";
	}
}
