package com.zettelnet.armorweight.event;

import org.bukkit.entity.Horse;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityEvent;

public class HorseWeightChangeEvent extends EntityEvent {

	private static final HandlerList handlers = new HandlerList();

	private double oldWeight, newWeight;

	public HorseWeightChangeEvent(final Horse horse, double oldWeight, double newWeight) {
		super(horse);

		this.oldWeight = oldWeight;
		this.newWeight = newWeight;
	}

	@Override
	public Horse getEntity() {
		return (Horse) super.getEntity();
	}

	public double getOldWeight() {
		return oldWeight;
	}

	public double getNewWeight() {
		return newWeight;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public String toString() {
		return "HorseWeightChangeEvent [horse=" + getEntity() + ", oldWeight=" + oldWeight + ", newWeight=" + newWeight + "]";
	}
}
