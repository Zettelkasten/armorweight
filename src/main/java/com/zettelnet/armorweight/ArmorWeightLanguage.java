package com.zettelnet.armorweight;

import org.bukkit.configuration.file.FileConfiguration;

import com.zettelnet.armorweight.zet.chat.ChatMessage;
import com.zettelnet.armorweight.zet.configuration.LanguageConfigurationFile;

public class ArmorWeightLanguage extends LanguageConfigurationFile {

	private final ArmorWeightPlugin plugin;

	public ChatMessage commandErrorSyntax;
	public ChatMessage commandErrorInvalidArgument;
	public ChatMessage commandErrorInvalidArgumentType;
	public ChatMessage commandErrorMissingArgument;
	public ChatMessage commandErrorNotPlayer;
	public ChatMessage commandErrorNoPermission;
	public ChatMessage commandErrorDisabled;

	public ChatMessage commandArgumentAmount;
	public ChatMessage commandArgumentPlayer;

	public ChatMessage commandHelpCommand;

	public ChatMessage weightGetSelf;
	public ChatMessage weightGetSelfHorse;
	public ChatMessage weightGetOther;
	public ChatMessage weightGetOtherHorse;
	public ChatMessage weightHelpGeneral;
	public ChatMessage weightHelpGet;
	public ChatMessage weightWarning;

	public ChatMessage pluginPrefix;
	public ChatMessage pluginInfo;
	public ChatMessage pluginWebsite;
	public ChatMessage pluginReload;
	public ChatMessage pluginUpdateAvailable;
	public ChatMessage pluginHelpReload;
	public ChatMessage pluginHelpDebug;
	public ChatMessage pluginHelpDebugUpdate;

	public ArmorWeightLanguage(ArmorWeightPlugin plugin, String file, String resource) {
		super(plugin, file, resource);
		this.plugin = plugin;
	}

	@Override
	public String getLanguage() {
		return plugin.getConfiguration().chatLanguage();
	}

	@Override
	public void loadValues(FileConfiguration config, FileConfiguration defaults) {
		if (config.getBoolean("config.autoUpdate", true)) {
			update(config);
		}

		pluginPrefix = load("plugin.prefix", "&(darkGray)[&(gray)ArmorWeight&(gray)] ");
		addFormatOption("prefix", pluginPrefix.toFormatOption());

		commandErrorSyntax = load("command.error.syntax", "&(red)Syntax: &(gray)/%(syntax)");
		commandErrorInvalidArgument = load("command.error.invalidArgument", "&(red)Argument \"%(arg)\" invalid");
		commandErrorInvalidArgumentType = load("command.error.invalidArgumentType", "&(red)Argument %(argType) \"%(arg)\" invalid");
		commandErrorMissingArgument = load("command.error.missingArgument", "&(red)Missing argument %(argType)");
		commandErrorNotPlayer = load("command.error.notPlayer", "&(red)The player %(player) is currently not online");
		commandErrorNoPermission = load("command.error.noPermission", "&(red)The player %(player) is currently not online");
		commandErrorDisabled = load("command.error.disabled", "&(red)You don't have permission to do this");

		commandArgumentAmount = load("command.argument.amount", "amount");
		commandArgumentPlayer = load("command.argument.player", "player");

		commandHelpCommand = load("command.help.command", "&(white)/%(syntax)&(white) - &(gray)%(description)");

		weightGetSelf = load("weight.get.self", "&(prefix)&(gray)You weigh &(gold)%(weight) &(yellow)(%(playerWeight) + %(armorWeight))&(gray)!");
		weightGetSelfHorse = load("weight.get.selfHorse", "&(prefix)&(gray)Your &(darkGray)horse &(gray)weighs &(gold)%(weight) &(yellow)(%(horseWeight) + %(armorWeight) + %(passengerWeight))&(gray)!");
		weightGetOther = load("weight.get.other", "&(prefix)&(gray)%(player) weighs &(gold)%(weight) &(yellow)(%(playerWeight) + %(armorWeight))&(gray)!");
		weightGetOtherHorse = load("weight.get.otherHorse", "&(prefix)&(gray)%(player)'s &(darkGray)horse &(gray)weighs &(gold)%(weight) &(yellow)(%(horseWeight) + %(armorWeight) + %(passengerWeight))&(gray)!");
		weightHelpGeneral = load("weight.help.general", "&(prefix)&(gray)Your &(yellow)weight &(gray)effects how &(yellow)fast &(gray)you can move.&(br)The more &(yellow)armor &(gray)you and your horse are wearing, the more your weight goes up.");
		weightHelpGet = load("weight.help.get", "Gets the weight of a player");
		weightWarning = load("weight.warning", "&(prefix)&(gray)Wearing heavy armor weighs you down");

		pluginInfo = load("plugin.info", "&(prefix)&(gray)This server is running &(darkGray)ArmorWeight&(gray) v%(version) by %(creator)!");
		pluginWebsite = load("plugin.website", "&(darkGray)Website: &(gray)%(website)");
		pluginReload = load("plugin.reload", "&(prefix)&(white)Reloaded configurations &(...)");
		pluginUpdateAvailable = load("plugin.updateAvailable", "&(prefix)&(gray)An update is available &(white)(%(updateName) for %(updateGameVersion))&(gray)!&(br)&(white)Download it at &(gray)%(updateLink)&(white).");
		pluginHelpReload = load("plugin.help.reload", "Reloads the plugin configurations");
		pluginHelpDebug = load("plugin.help.debug", "Prints debug information");
		pluginHelpDebugUpdate = load("plugin.help.debugUpdate", "Force recalculation of your weight");
	}

	@Override
	public void update(FileConfiguration config) {
		switch (getVersion()) {
		default:
			plugin.getLogger().warning("Unknown version of configuration file \"lang.yml\". Will not update file");
			break;
		case "unknown":
		case "0.1.0":
			updateStart("0.1.0");
			setIfNotExists("enUS.weight.get.selfHorse", "&(prefix)&(gray)Your &(darkGray)horse &(gray)weighs &(gold)%(weight) &(yellow)(%(horseWeight) + %(armorWeight) + %(passengerWeight))&(gray)!");
			setIfNotExists("enUS.weight.get.otherHorse", "&(prefix)&(gray)%(player)'s &(darkGray)horse &(gray)weighs &(gold)%(weight) &(yellow)(%(horseWeight) + %(armorWeight) + %(passengerWeight))&(gray)!");
		case "0.2.0":
		case "0.2.1":
		case "0.3.0":
		case "0.3.1":
		case "0.3.2":
			updateStart("0.3.2");
			setIfNotExists("enUS.weight.warning", "&(prefix)&(gray)Wearing heavy armor will cause you to slow down!");
		case "0.3.3":
		case "0.3.4":
		case "0.3.5":
			updateStart("0.3.5");
			setIfNotExists("enUS.weight.help.help.general", "&(prefix)&(gray)Your &(yellow)weight &(gray)effects how &(yellow)fast &(gray)you can move.&(br)The more &(yellow)armor &(gray)you and your horse are wearing, the more your weight goes up.");
		case "0.3.6":
			updateStart("0.3.6");
			setIfNotExists("enUS.plugin.help.debug", "Prints debug information");
		case "0.3.7":
		case "0.3.8":
		case "0.3.9":
			updateStart("0.3.9");
			setIfNotExists("enUS.plugin.help.debugUpdate", "Force recalculation of your weight");
		case "0.3.10":
		case "0.3.11":
		case "0.3.12":
		case "0.3.13":
		case "0.3.14":
		case "0.3.15":
		case "0.3.16":
			updateDone("0.3.16");
		}
	}

	@Deprecated
	public ChatMessage pluginPrefix() {
		return pluginPrefix;
	}
}
