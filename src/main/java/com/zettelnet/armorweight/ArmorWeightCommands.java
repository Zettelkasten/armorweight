package com.zettelnet.armorweight;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;

public class ArmorWeightCommands implements CommandExecutor {

	private final ArmorWeightPlugin plugin;

	public ArmorWeightCommands(ArmorWeightPlugin plugin) {
		this.plugin = plugin;

		plugin.getCommand("weight").setExecutor(this);
		plugin.getCommand("weight").setTabCompleter(new ArmorWeightTabCompleter());
		plugin.getCommand("armorweight").setExecutor(this);
		plugin.getCommand("armorweight").setTabCompleter(new ArmorWeightTabCompleter());
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		ArmorWeightLanguage lang = plugin.getLanguage();

		switch (command.getName().toLowerCase()) {
		case "weight":
			if (!sender.hasPermission("armorweight.command.weight.getown")) {
				lang.commandErrorNoPermission.send(sender);
				return true;
			}
			if (args.length == 0) {
				if (!(sender instanceof Player)) {
					lang.commandErrorMissingArgument.send(sender, "argType", lang.commandArgumentPlayer);
					lang.commandErrorSyntax.send(sender, "syntax", "weight <" + lang.commandArgumentPlayer + ">");
					return true;
				}
				Player p = (Player) sender;
				WeightManager wm = plugin.getWeightManager();
				if (!(p.getVehicle() instanceof Horse)) {
					lang.weightGetSelf.send(sender, "player", p.getName(), "weight", wm.formatWeight(wm.getWeight(p)), "playerWeight", wm.formatWeight(wm.getPlayerWeight(p)), "armorWeight", wm.formatWeight(wm.getArmorWeight(p)));
				} else {
					Horse h = (Horse) p.getVehicle();
					lang.weightGetSelfHorse.send(sender, "player", p.getName(), "weight", wm.formatWeight(wm.getWeight(h)), "horseWeight", wm.formatWeight(wm.getHorseWeight(h)), "passengerWeight", wm.formatWeight(wm.isHorsePassengerWeightEnabled() ? wm.getWeight(p) : 0D), "armorWeight", wm.formatWeight(wm.getArmorWeight(h)));
				}
				return true;
			}
			if (args[0].equalsIgnoreCase("help")) {
				lang.weightHelpGeneral.send(sender);
				return true;
			}
			if (args[0].equalsIgnoreCase("update")) {
				if (!sender.hasPermission("armorweight.command.plugin.debug")) {
					lang.commandErrorNoPermission.send(sender);
					return true;
				}
				WeightManager wm = plugin.getWeightManager();
				if (!(sender instanceof Player)) {
					lang.commandErrorNoPermission.send(sender);
					return true;
				}
				Player p = (Player) sender;

				p.sendMessage("Previous weight: " + wm.getWeight(p));
				p.sendMessage("Updating " + (wm.updateWeight(p) ? (ChatColor.GREEN + "changed") : (ChatColor.RED + "did not change")) + " weight");
				p.sendMessage("Weight after update: " + wm.getWeight(p));
				wm.updateEffects(p);
				p.sendMessage("Updated effects.");

				return true;
			}
			if (!sender.hasPermission("armorweight.command.weight.getothers")) {
				lang.commandErrorNoPermission.send(sender);
				return true;
			}
			// getting player by name is fine as we are using commands and won't
			// save anything
			@SuppressWarnings("deprecation")
			Player p = Bukkit.getPlayer(args[0]);
			if (p == null) {
				lang.commandErrorNotPlayer.send(sender, "player", args[0]);
				if (sender instanceof Player) {
					lang.commandErrorSyntax.send(sender, "syntax", "weight [" + lang.commandArgumentPlayer + "]");
				} else {
					lang.commandErrorSyntax.send(sender, "syntax", "weight <" + lang.commandArgumentPlayer + ">");
				}
				return true;
			}
			WeightManager wm = plugin.getWeightManager();
			if (!(p.getVehicle() instanceof Horse)) {
				lang.weightGetOther.send(sender, "player", p.getName(), "weight", wm.formatWeight(wm.getWeight(p)), "playerWeight", wm.formatWeight(wm.getPlayerWeight(p)), "armorWeight", wm.formatWeight(wm.getArmorWeight(p)));
			} else {
				Horse h = (Horse) p.getVehicle();
				lang.weightGetOtherHorse.send(sender, "player", p.getName(), "weight", wm.formatWeight(wm.getWeight(h)), "horseWeight", wm.formatWeight(wm.getHorseWeight(h)), "passengerWeight", wm.formatWeight(wm.isHorsePassengerWeightEnabled() ? wm.getWeight(p) : 0D), "armorWeight", wm.formatWeight(wm.getArmorWeight(h)));
			}
			return true;
		case "armorweight":
			if (!sender.hasPermission("armorweight.command.plugin.info")) {
				lang.commandErrorNoPermission.send(sender);
				return true;
			}
			if (args.length == 0) {
				lang.pluginInfo.send(sender, "version", plugin.getVersion(), "creator", "Zettelkasten");
				lang.pluginWebsite.send(sender, "website", plugin.getWebsite());
				return true;
			}
			switch (args[0].toLowerCase()) {
			case "help":
				if (sender.hasPermission("armorweight.command.weight.getown")) {
					if (sender instanceof Player) {
						lang.commandHelpCommand.send(sender, "syntax", "weight [" + lang.commandArgumentPlayer + "]", "description", lang.weightHelpGet);
					} else {
						lang.commandHelpCommand.send(sender, "syntax", "weight <" + lang.commandArgumentPlayer + ">", "description", lang.weightHelpGet);
					}
				}
				if (sender.hasPermission("armorweight.command.plugin.debug") && sender instanceof Player) {
					lang.commandHelpCommand.send(sender, "syntax", "weight update", "description", lang.pluginHelpDebugUpdate);
				}
				if (sender.hasPermission("armorweight.command.plugin.reload")) {
					lang.commandHelpCommand.send(sender, "syntax", "armorweight reload", "description", lang.pluginHelpReload);
				}
				if (sender.hasPermission("armorweight.command.plugin.debug")) {
					lang.commandHelpCommand.send(sender, "syntax", "armorweight debug", "description", lang.pluginHelpDebug);
				}
				return true;
			case "reload":
			case "rl":
				if (!sender.hasPermission("armorweight.command.plugin.reload")) {
					lang.commandErrorNoPermission.send(sender);
					return true;
				}
				plugin.reload();
				lang.pluginReload.send(sender);
				return true;
			case "debug":
				if (!sender.hasPermission("armorweight.command.plugin.debug")) {
					lang.commandErrorNoPermission.send(sender);
					return true;
				}
				Server server = plugin.getServer();
				wm = plugin.getWeightManager();
				sender.sendMessage("Running " + plugin.getName() + " " + plugin.getVersion() + " on " + server.getName() + " " + server.getVersion());
				sender.sendMessage("Loaded players: " + wm.getLoadedPlayers().size() + " (of " + server.getOnlinePlayers().size() + " total)");
				sender.sendMessage("Loaded horses: " + wm.getLoadedHorses().size());
				sender.sendMessage("Using PortableHorses: " + wm.isPortableHorsesEnabled());
				return true;
			default:
				lang.commandErrorInvalidArgument.send(sender, "arg", args[0]);
				lang.commandErrorSyntax.send(sender, "syntax", "armorweight help");
				return true;
			}
		default:
			return true;
		}
	}

	public class ArmorWeightTabCompleter implements TabCompleter {

		@Override
		public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
			int length = args.length == 0 ? 1 : args.length;

			switch (command.getName().toLowerCase()) {
			case "weight":
				if (!sender.hasPermission("armorweight.command.weight.getown") || !sender.hasPermission("armorweight.command.weight.getothers")) {
					return null;
				}
				List<String> options = new ArrayList<>(Bukkit.getOnlinePlayers().size());
				for (Player player : Bukkit.getOnlinePlayers()) {
					options.add(player.getName());
				}
				if (sender.hasPermission("armorweight.command.plugin.debug")) {
					options.add("update");
				}
				return options;
			case "armorweight":
				switch (length) {
				case 1:
					options = new ArrayList<>(2);
					options.add("help");
					if (sender.hasPermission("armorweight.command.plugin.reload")) {
						options.add("reload");
					}
					if (sender.hasPermission("armorweight.command.plugin.debug")) {
						options.add("debug");
					}
					return options;
				default:
					return null;
				}
			default:
				return null;
			}
		}
	}
}
