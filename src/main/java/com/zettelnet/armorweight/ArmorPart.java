package com.zettelnet.armorweight;

import org.apache.commons.lang.Validate;

public enum ArmorPart {

	BOOTS(0), LEGGINGS(1), CHESTPLATE(2), HELMET(3);

	private final int id;

	private double weightShare;

	private ArmorPart(int id) {
		this.id = id;
		this.weightShare = 0.25F;
	}

	public int getId() {
		return id;
	}

	public static ArmorPart valueOf(int id) {
		switch (id) {
		case 0:
			return BOOTS;
		case 1:
			return LEGGINGS;
		case 2:
			return CHESTPLATE;
		case 3:
			return HELMET;
		default:
			return null;
		}
	}

	public double getWeightShare() {
		return weightShare;
	}

	public void setWeightShare(double weightShare) {
		this.weightShare = weightShare;
	}

	public static ArmorPart matchPart(String name) {
		Validate.notNull(name, "Name cannot be null");
		return ArmorPart.valueOf(name.toUpperCase().replaceAll("\\s+", "_").replaceAll("\\W", ""));
	}
}
