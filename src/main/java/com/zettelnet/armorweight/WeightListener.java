package com.zettelnet.armorweight;

import org.bukkit.Server;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import net.ess3.api.IEssentials;

public class WeightListener implements Listener {

	private WeightManager manager;

	public WeightListener(WeightManager manager) {
		this.manager = manager;
	}

	public WeightManager getManager() {
		return manager;
	}

	public void setManager(WeightManager manager) {
		this.manager = manager;
	}	

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerJoin(PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		if (manager.isWorldEnabled(player.getWorld())) {
			manager.loadPlayer(player);

			// Essentials fix
			// ==============
			// Basically, Essentials resets the speed for all players not having
			// the permission "essentials.speed" on join
			// This is an ugly fix that updates the effects of the player
			// multiple times after they joined
			Plugin plugin = manager.getPlugin();
			Server server = plugin.getServer();
			if (server.getPluginManager().isPluginEnabled("Essentials")) {
				// Essentials installed
				final IEssentials ess = (IEssentials) server.getPluginManager().getPlugin("Essentials");
				if (!ess.getUser(player).isAuthorized("essentials.speed")) {
					new BukkitRunnable() {
						private int tick = 0;

						@Override
						public void run() {
							if (tick == 4) {
								this.cancel();
							}

							// Player is loaded
							manager.updateEffects(player);

							tick++;

						}
					}.runTaskTimer(plugin, 0, 2);
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		manager.unloadPlayer(player);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		final Player player = event.getPlayer();
		manager.updateWeight(player);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
		Player player = event.getPlayer();
		if (manager.isWorldEnabled(player.getWorld())) {
			manager.loadPlayer(player);
			
			// fix for some plugins that reset player speed on world change
			manager.updateEffects(player);
		} else {
			manager.unloadPlayer(player);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		manager.updateWeightLater(player);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onInventoryClick(InventoryClickEvent event) {
		onInventoryInteract(event);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onInventoryDrag(InventoryDragEvent event) {
		onInventoryInteract(event);
	}

	// ignoreCancelled = true
	public void onInventoryInteract(InventoryInteractEvent event) {
		InventoryHolder holder = event.getInventory().getHolder();
		if (holder instanceof Player) {
			manager.updateWeightLater((Player) holder);
		}
		if (holder instanceof Horse && manager.isHorseLoaded((Horse) holder)) {
			final Horse horse = (Horse) holder;
			manager.updateWeightLater(horse);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onInventoryClickPortableHorsesFix(InventoryClickEvent event) {
		if (!manager.isPortableHorsesEnabled()) {
			return;
		}
		InventoryHolder holder = event.getInventory().getHolder();
		if (holder instanceof Player) {
			manager.updateEffects((Player) holder);
		}
		Player player = (Player) event.getWhoClicked();
		if (!(holder instanceof Horse) || (!manager.isHorseLoaded((Horse) holder))) {
			return;
		}
		Horse horse = (Horse) holder;
		try {
			if (manager.isKickedOffHorse(player, horse, event.getCurrentItem())) {
				manager.unloadHorse(horse, null);
			}
		} catch (Exception e) {
			manager.getPlugin().getLogger().warning("Failed to perform PortableHorses fix:");
			e.printStackTrace();
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onInventoryDragPortableHorsesFix(InventoryDragEvent event) {
		if (!manager.isPortableHorsesEnabled()) {
			return;
		}
		InventoryHolder holder = event.getInventory().getHolder();
		if (holder instanceof Player) {
			manager.updateEffects((Player) holder);
		}
		Player player = (Player) event.getWhoClicked();
		if (!(holder instanceof Horse) || (!manager.isHorseLoaded((Horse) holder))) {
			return;
		}
		Horse horse = (Horse) holder;
		try {
			for (ItemStack item : event.getNewItems().values()) {
				if (manager.isKickedOffHorse(player, horse, item)) {
					manager.unloadHorse(horse, null);
				}
			}
		} catch (Exception e) {
			manager.getPlugin().getLogger().warning("Failed to perform PortableHorses fix:");
			e.printStackTrace();
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onVehicleEnter(VehicleEnterEvent event) {
		if (event.getVehicle() instanceof Horse) {
			manager.loadHorse((Horse) event.getVehicle(), event.getEntered());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onVehicleExit(VehicleExitEvent event) {
		if (event.getVehicle() instanceof Horse) {
			manager.unloadHorse((Horse) event.getVehicle(), null);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerGameModeChange(final PlayerGameModeChangeEvent event) {
		final Player player = event.getPlayer();
		manager.updateWeightLater(player, true);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerVelocity(PlayerVelocityEvent event) {
		manager.updateKnockbackEffect(event.getPlayer(), event.getVelocity());
	}
}
