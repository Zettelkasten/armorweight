package com.zettelnet.armorweight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.plugin.Plugin;

import com.google.common.base.CaseFormat;
import com.zettelnet.armorweight.zet.configuration.PluginConfigurationFile;

public class ArmorWeightConfiguration extends PluginConfigurationFile {

	private final Plugin plugin;
	private final WeightManager manager;

	private boolean metricsEnabled;
	private String chatLanguage;

	private boolean weightWarningEnabled;
	private long weightWarningCooldown;

	private final Map<String, Enchantment> enchantmentMappings;

	public ArmorWeightConfiguration(Plugin plugin, String file, String resource, WeightManager manager) {
		super(plugin, file, resource);
		this.plugin = plugin;
		this.manager = manager;
		enchantmentMappings = new HashMap<>();

		try {
			// order of this list IS IMPORTANT! for supporting older versions of
			// Minecraft that don't have all of these enchantments; newer
			// enchantments are further down the list
			enchantmentMappings.put("protection", Enchantment.PROTECTION_ENVIRONMENTAL);
			enchantmentMappings.put("fireProtection", Enchantment.PROTECTION_FIRE);
			enchantmentMappings.put("featherFalling", Enchantment.PROTECTION_FALL);
			enchantmentMappings.put("blastProtection", Enchantment.PROTECTION_EXPLOSIONS);
			enchantmentMappings.put("projectileProtection", Enchantment.PROTECTION_PROJECTILE);
			enchantmentMappings.put("respiration", Enchantment.OXYGEN);
			enchantmentMappings.put("aquaAffinity", Enchantment.WATER_WORKER);
			enchantmentMappings.put("sharpness", Enchantment.DAMAGE_ALL);
			enchantmentMappings.put("smite", Enchantment.DAMAGE_UNDEAD);
			enchantmentMappings.put("baneOfArthropods", Enchantment.DAMAGE_ARTHROPODS);
			enchantmentMappings.put("knockback", Enchantment.KNOCKBACK);
			enchantmentMappings.put("fireAspect", Enchantment.FIRE_ASPECT);
			enchantmentMappings.put("looting", Enchantment.LOOT_BONUS_MOBS);
			enchantmentMappings.put("efficiency", Enchantment.DIG_SPEED);
			enchantmentMappings.put("silkTouch", Enchantment.SILK_TOUCH);
			enchantmentMappings.put("unbreaking", Enchantment.DURABILITY);
			enchantmentMappings.put("fortune", Enchantment.LOOT_BONUS_BLOCKS);
			enchantmentMappings.put("power", Enchantment.ARROW_DAMAGE);
			enchantmentMappings.put("punch", Enchantment.ARROW_KNOCKBACK);
			enchantmentMappings.put("flame", Enchantment.ARROW_FIRE);
			enchantmentMappings.put("infinity", Enchantment.ARROW_INFINITE);
			enchantmentMappings.put("thorns", Enchantment.THORNS);
			enchantmentMappings.put("luckOfTheSea", Enchantment.LUCK);
			enchantmentMappings.put("lure", Enchantment.LURE);
			enchantmentMappings.put("frostWalker", Enchantment.FROST_WALKER);
			enchantmentMappings.put("mending", Enchantment.MENDING);
			enchantmentMappings.put("curseOfBinding", Enchantment.BINDING_CURSE);
			enchantmentMappings.put("curseOfVanishing", Enchantment.VANISHING_CURSE);
			enchantmentMappings.put("sweepingEdge", Enchantment.SWEEPING_EDGE);
			enchantmentMappings.put("channeling", Enchantment.CHANNELING);
			enchantmentMappings.put("impaling", Enchantment.IMPALING);
			enchantmentMappings.put("loyalty", Enchantment.LOYALTY);
			enchantmentMappings.put("riptide", Enchantment.RIPTIDE);
		} catch (NoSuchFieldError e) {
			// ignore
		}
	}

	@Override
	protected synchronized void loadValues(FileConfiguration config) {
		if (config.getBoolean("config.autoUpdate", true)) {
			update(config);
		}

		metricsEnabled = config.getBoolean("metricsEnabled", false);

		WeightManager.PLAYER_WEIGHT = config.getDouble("playerWeight", 90) / 100D;
		WeightManager.HORSE_WEIGHT = config.getDouble("horseWeight", 400) / 100D;

		manager.setPlayerArmorWeightEnabled(config.getBoolean("weightEnabled.armor.player", true));
		manager.setHorseArmorWeightEnabled(config.getBoolean("weightEnabled.armor.horse", true));
		manager.setHorsePassengerWeightEnabled(config.getBoolean("weightEnabled.horseRider", true));
		manager.setEnchantmentWeightEnabled(config.getBoolean("weightEnabled.enchantment", false));

		manager.setPlayerSpeedEffectEnabled(config.getBoolean("effectEnabled.speed.player", true));
		manager.setPlayerCreativeSpeedEffectEnabled(config.getBoolean("effectEnabled.speed.playerCreative", false));
		// The default value is "false" to avoid bugged horses if the key was
		// not found
		manager.setHorseSpeedEffectEnabled(config.getBoolean("effectEnabled.speed.horse", false));
		manager.setSpeedEffectAmplifier(config.getDouble("effectEnabled.speed.amplifier", 1.0));
		
		manager.setPlayerKnockbackEffectEnabled(config.getBoolean("effectEnabled.knockback.player", true));
		manager.setKnockbackEffectAmplifier(config.getDouble("effectEnabled.knockback.amplifier", 1.0));

		manager.setEnabledWorlds(new HashSet<String>());
		manager.setAllWorldsEnabled(false);
		if (config.isList("enabledWorlds")) {
			for (Object obj : config.getList("enabledWorlds", new ArrayList<>())) {
				if (obj instanceof String) {
					String worldName = (String) obj;
					if (worldName.equals("*")) {
						manager.setAllWorldsEnabled(true);
					} else {
						manager.enableWorld((String) obj);
					}
				} else {
					plugin.getLogger().warning("Could not enable world " + obj + "; not a String as world name");
				}
			}
		} else {
			plugin.getLogger()
					.warning("Invalid configuration for \"enabledWorlds\" (should be a list); ENABLING ALL WORLDS");
			manager.setAllWorldsEnabled(true);
		}

		ArmorType.reset();
		if (config.isConfigurationSection("armor.weight")) {
			for (Entry<String, Object> entry : config.getConfigurationSection("armor.weight").getValues(false)
					.entrySet()) {
				loadArmorTypeWeight(entry.getKey(), entry.getValue());
			}
		} else {
			plugin.getLogger().warning(
					"Invalid configuration for \"armor.weight\" (should be a section); not loading armor weights");
		}

		double helmetShare = config.getDouble("armor.share.helmet", 17);
		double chestplateShare = config.getDouble("armor.share.chestplate", 45);
		double leggingsShare = config.getDouble("armor.share.leggings", 25);
		double bootsShare = config.getDouble("armor.share.boots", 13);
		double shareSum = helmetShare + chestplateShare + leggingsShare + bootsShare;
		ArmorPart.CHESTPLATE.setWeightShare(chestplateShare / shareSum);
		ArmorPart.LEGGINGS.setWeightShare(leggingsShare / shareSum);
		ArmorPart.HELMET.setWeightShare(helmetShare / shareSum);
		ArmorPart.BOOTS.setWeightShare(bootsShare / shareSum);

		chatLanguage = config.getString("chat.language", "enUS");

		WeightManager.DEFAULT_ENCHANTMENT_WEIGHT = 0D;
		WeightManager.ENCHANTMENT_WEIGHTS.clear();
		if (manager.isEnchantmentWeightEnabled()) {
			if (config.isConfigurationSection("enchantment.weight")) {
				for (Entry<String, Object> entry : config.getConfigurationSection("enchantment.weight").getValues(false)
						.entrySet()) {
					loadEnchantmentWeight(entry.getKey(), entry.getValue());
				}
			} else {
				manager.setEnchantmentWeightEnabled(false);
				plugin.getLogger().warning(
						"Invalid configuration for \"enchantment.weight\" (should be a section); not using enchantments weights");
			}
		}

		weightWarningEnabled = config.getBoolean("weightWarning.enabled", true);
		weightWarningCooldown = (long) (config.getDouble("weightWarning.cooldown", 10D) * 1000);
	}

	private boolean loadArmorTypeWeight(String name, Object value) {
		if (ArmorType.contains(name)) {
			try {
				ArmorType.valueOf(name).setWeight(Double.valueOf(value.toString()) / 100);
				return true;
			} catch (NumberFormatException e) {
				plugin.getLogger().warning("Armor weight value for " + name + " is an invalid number; ignoring it");
				return false;
			}
		}
		Material material = Material.matchMaterial(name);
		if (material == null) {
			plugin.getLogger()
					.warning("Armor weight key for " + name + " is not a material or armor type; ignoring it");
			return false;
		}
		try {
			ArmorType type = new ArmorType(material);
			type.setWeight(Double.valueOf(value.toString()) / 100);
			ArmorType.register(type);
			return true;
		} catch (NumberFormatException e) {
			plugin.getLogger().warning("Armor weight value for " + name + " is an invalid number; ignoring it");
			return false;
		}
	}

	private boolean loadEnchantmentWeight(String name, Object value) {
		if (name.equalsIgnoreCase("general")) {
			try {
				WeightManager.DEFAULT_ENCHANTMENT_WEIGHT = Double.valueOf(value.toString()) / 100;
				return true;
			} catch (NumberFormatException e) {
				plugin.getLogger()
						.warning("Enchantment weight value for " + name + " is an invalid number; ignoring it");
				return false;
			}
		}

		NamespacedKey key = NamespacedKey.minecraft(CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, name));
		Enchantment ench = Enchantment.getByKey(key);
		if (ench == null) {
			ench = enchantmentMappings.get(name);
			if (ench == null) {
				plugin.getLogger().warning("Unknown enchantment type \"" + name + "\"; ignoring it");
				return false;
			}
		}

		try {
			WeightManager.ENCHANTMENT_WEIGHTS.put(ench, Double.valueOf(value.toString()) / 100);
			return true;
		} catch (NumberFormatException e) {
			plugin.getLogger().warning("Enchantment weight value for " + name + " is an invalid number; ignoring it");
			return false;
		}
	}

	@Override
	protected void update(FileConfiguration config) {
		switch (getVersion()) {
		default:
			plugin.getLogger().warning("Unknown version of configuration file \"config.yml\". Will not update file");
			break;
		case "unknown":
		case "0.1.0":
			setIfNotExists("horseWeight", 400);
		case "0.2.0":
			updateStart("0.2.0");
			setIfNotExists("enchantment.enabled", false);
			setIfNotExists("enchantment.weight.general", 1);
			setIfNotExists("enchantment.weight.protection", 2);
			setIfNotExists("enchantment.weight.unbreaking", 2);
		case "0.2.1":
			updateStart("0.2.1");
			setIfNotExists("weightEnabled.armor.player", true);
			setIfNotExists("weightEnabled.armor.horse", true);
			setIfNotExists("weightEnabled.horseRider", true);
			boolean enchantmentEnabled = config.getBoolean("enchantment.enabled", true);
			setIfNotExists("weightEnabled.enchantment", enchantmentEnabled);
			if (config.contains("enchantment.enabled")) {
				config.set("enchantment.enabled", null);
			}

			setIfNotExists("effectEnabled.speed.player", true);
			setIfNotExists("effectEnabled.speed.horse", true);

			setIfNotExists("enabledWorlds", Arrays.asList("world", "world_nether", "world_the_end", "*"));
		case "0.3.0":
		case "0.3.1":
			updateStart("0.3.1");
			setIfNotExists("effectEnabled.speed.playerCreative", false);
		case "0.3.2":
			updateStart("0.3.2");
			setIfNotExists("weightWarning.enabled", true);
			setIfNotExists("weightWarning.cooldown", 10);
		case "0.3.3":
		case "0.3.4":
		case "0.3.5":
		case "0.3.6":
		case "0.3.7":
		case "0.3.8":
		case "0.3.9":
		case "0.3.10":
		case "0.3.11":
		case "0.3.12":
		case "0.3.13":
		case "0.3.14":
			updateStart("0.3.14");
			setIfNotExists("effectEnabled.speed.amplifier", 1.0);
			setIfNotExists("effectEnabled.knockback.amplifier", 1.0);
			setIfNotExists("effectEnabled.knockback.player", true);
		case "0.3.15":
		case "0.3.16":
			updateDone("0.3.16");
		}
	}

	public String chatLanguage() {
		return chatLanguage;
	}

	public boolean metricsEnabled() {
		return metricsEnabled;
	}

	public boolean weightWarningEnabled() {
		return weightWarningEnabled;
	}

	public long weightWarningCooldown() {
		return weightWarningCooldown;
	}
}
